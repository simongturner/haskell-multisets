# README #

A Haskell module with functions for implementing and using multisets. Similar to Data.Set except less efficient and slightly more mathematically correct (doesn't require orderable items).

### How do I get set up? ###

* Install the Haskell platform with GHC and GHCI
* Use as a module with another Haskell program, or run with ghci for testing purposes. 

### Contribution guidelines ###

* Feel free to add anything you like and submit a pull request.