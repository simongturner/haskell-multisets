module Bags(listToBag,
            bagInsert,
            bagEqual,
            bagSum,
            bagIntersection)
where

    -- each item is a tuple of a polymorphic type and the count of that element
    type Item a = (Int, a)
    -- each bag is a list of items
    type Bag a = [Item a]

    -- Takes a list and inserts each element into it, returning a Bag
    listToBag :: (Eq a) => [a] -> Bag a
    listToBag [] = []
    listToBag (h:t) = bagInsert h (listToBag t)

    -- Inserts a thing of some type into a Bag
    bagInsert :: (Eq a) => a -> Bag a -> Bag a
    bagInsert a [] = [(1,a)]
    bagInsert a ((count,h):t)
        | a == h = (count + 1, h):t
        | otherwise = (count,h):bagInsert a t

    -- Inserts a specific number of things (in the form of a tuple (count,a)) into a Bag.
    bagInsertTuple :: (Eq a) => Item a -> Bag a -> Bag a
    bagInsertTuple (count,a) [] = [(count,a)]
    bagInsertTuple (count,a) ((bagCount,h):t)
        | a == h = (bagCount + count, h):t
        | otherwise = (bagCount,h):bagInsertTuple (count,a) t

    -- Inserts a Bag of things into another Bag.
    bagInsertBag :: (Eq a) => Bag a -> Bag a -> Bag a
    bagInsertBag [(count,a)] [] = bagInsertTuple (count,a) []
    bagInsertBag [] [x] = [x]
    bagInsertBag [(count,x)] (h:t) = bagInsertTuple (count,x) (h:t)
    bagInsertBag (h:t) [(count,y)] = bagInsertTuple (count,y) (h:t)
    bagInsertBag (h:t) (h2:t2) = bagInsertTuple h (bagInsertBag t (h2:t2))

    -- Checks whether two bags are equal.
    bagEqual :: (Eq a) => Bag a -> Bag a -> Bool
    bagEqual [] [] = True
    bagEqual [] _ = False
    bagEqual _ [] = False
    -- if bag1 is a subset of bag2 and bag2 is a subset of bag1, the bags must be equal
    bagEqual bag1 bag2 = bag1 `isSubsetOf` bag2 && bag2 `isSubsetOf` bag1

    -- Checks whether a bag is contained within another bag.
    isSubsetOf :: (Eq a) => Bag a -> Bag a -> Bool
    isSubsetOf [] _ = False
    isSubsetOf _ [] = False
    -- checks whether every element of bag1 is an element of bag2
    isSubsetOf bag1 bag2 = and [item `elem` bag2 | item <- bag1]

    -- Adds the contents of two bags together.
    bagSum :: (Eq a) => Bag a -> Bag a -> Bag a
    bagSum [] [x] = [x]
    bagSum [x] [] = [x]
    bagSum [(count,x)] (h:t) = bagInsertTuple (count,x) (h:t)
    bagSum (h:t) [(count,y)] = bagInsertTuple (count,y) (h:t)
    bagSum (h:t) (h2:t2) = bagInsertTuple h2 (bagInsertTuple h (bagInsertBag t t2))

    -- Returns the intersection of two Bags
    bagIntersection :: (Eq a) => Bag a -> Bag a -> Bag a
    bagIntersection [] x = x
    bagIntersection x [] = x
    bagIntersection [x] (h:t) = bagIntersectionA [x] (h:t)
    bagIntersection (h:t) [x] = bagIntersectionA (h:t) [x]
    bagIntersection (x:xs) (y:ys) = bagIntersection [x] (bagIntersection [y] (bagIntersection xs ys))

    -- recurses through a bag, only keeping the lowest count tuple of any particular element.
    bagIntersectionA :: (Eq a) => Bag a -> Bag a -> Bag a
    bagIntersectionA [(i,x)] ((j,y):ys)
        | x == y = (min i j, x):ys
        | otherwise = (j,y):bagIntersectionA [(i,x)] ys
    bagIntersectionA ((j,y):ys) [(i,x)]
        | x == y = (min i j, x):ys
        | otherwise = (j,y):bagIntersectionA [(i,x)] ys
